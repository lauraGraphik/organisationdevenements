<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Repository\EventRepository;
use App\Entity\Event;
use App\Entity\User;
use App\Form\UserType;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="app_index")
     */
    public function index(EventRepository $repo): Response
    {
        // Tous les events
        $events=$repo->findAll();

        // Tous les events du plus vieux au plus lointain
        $events=$repo->findBy([], ["dateEvent"=>"ASC"]);

        // On crée la fonciton findNext dans le repo pour n'afficher que les évenements futurs
        $events=$repo->findNext();

        return $this->render('front/index.html.twig', [
            'events' => $events,
        ]);
    }

    /**
     * @Route("/evenement/{id}", name="app_event")
     */
    public function event(Event $event): Response
    {
        return $this->render('front/event.html.twig', [
            'event' => $event,
        ]);
    }

    /**
     * @Route("/evenement/{id}/inscription", name="app_sub_event")
     * @IsGranted("ROLE_USER")
     */
    public function eventSub(Event $event): Response
    {
        // on teste s'il reste des places ou si c'est illimité ( càd maxPublic = null )
        if($event->getMaxPublic() and $event->getSubscribers()->count() == $event->getMaxPublic()){ // plus de place
            $message = "Nous sommes désolés, l'événement est complet ! Votre inscription n'a pas pu être prise en compte.";
            $res="error";
        }else{
            $event->addSubscriber($this->getUser());
            $this->getDoctrine()->getManager()->flush();
            $message = "Votre inscription a bien été prise en compte !";
            $res="success";
        }
        return $this->render('front/event.html.twig', [
            'event' => $event,
            'message'=>$message,
            'res'=>$res
        ]);
    }

    /**
     * @Route("/evenement/{id}/desinscription", name="app_unsub_event")
     * @IsGranted("ROLE_USER")
     */
    public function eventUnSub(Event $event): Response
    {
       
        $event->removeSubscriber($this->getUser());
        $this->getDoctrine()->getManager()->flush();
        $message = "Vous avez bien été désinscrit de l'événement !";
        $res="success";
      
        return $this->render('front/event.html.twig', [
            'event' => $event,
            'message'=>$message,
            'res'=>$res
        ]);
    }

    
    /**
     * @Route("/evenements", name="app_events")
     */
    public function events(EventRepository $repo): Response
    {
        return $this->render('front/events.html.twig', [
            'eventsNext' => $repo->findNext(),
            'eventsPrev' => $repo->findPrev()
        ]);
    }

    /**
     * @Route("/mon-compte", name="app_profile")
     */
    public function profile(EventRepository $repo): Response
    {
        $user=$this->getUser();
        return $this->render('front/profile.html.twig',
            ['eventsNext' => $repo->findNextForUser($user),
            'eventsPrev' => $repo->findPrevForUser($user)]
        );
    }

    /**
     * @Route("/inscription", name="user_new", methods={"GET","POST"})
     */
    public function newUser(UserPasswordEncoderInterface $encoder, Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, ["isAdmin"=>$this->isGranted("ROLE_ADMIN")]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword( $encoder->encodePassword($user, $user->getPassword()) );
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            if( $this->isGranted("ROLE_ADMIN")) return $this->redirectToRoute('user_index');

            return $this->redirectToRoute('app_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

}
