<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Event;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder=$encoder;
    }
    public function load(ObjectManager $manager)
    {
        $users=[
            ["username"=>"Admin", "roles"=>["ROLE_ADMIN"], "password"=>"mdp"],
            ["username"=>"Insrit", "roles"=>["ROLE_USER"], "password"=>"mdp"]
        ];

        $events=[
            [
                "title"=>"Stage de crochet",
                "dateEvent"=>new \Datetime("2021-02-10 18:00", new \DateTimeZone('Europe/Paris')),
                "description"=>"<h2>Apprenez à créer un magnifique coeur en crochet.</h2> 
                    <p>Votre moitié sera heureux (heureuse) de recevoir ce magnifique cadeau pour la saint Valentin !!</p>
                    <p>
                    Je ne voudrais pas rentrer <strong>dans des choses trop dimensionnelles</strong>, mais, là on voit qu'on a beaucoup à travailler sur nous-mêmes car là, j'ai un chien en ce moment à côté de moi et je le caresse, et cela même si les gens ne le savent pas ! Mais ça, c'est uniquement lié au spirit.</p>
                    <p>
                    Quand tu fais le calcul, je sais que, grâce à ma propre vérité c'est juste une question d'awareness et ça, c'est très dur, et, et, et... c'est très facile en même temps. Et tu as envie de le dire au monde entier, including yourself.
                    </p><p>
                    Même si on se ment, je ne suis pas un simple danseur car c'est un très, très gros travail car l'aboutissement de l'instinct, c'est l'amour ! C'est cette année que j'ai eu la révélation !</p>
                    ",
                "price"=>15,
                "maxPublic"=>8,
                "image"=>"crochet.jpg"
            ],
            [
                "title"=>"Halloween",
                "dateEvent"=>new \Datetime("2020-10-21 18:00"),
                "description"=>"Effrayez vos voisins en buvant du jus de citrouille !",
                "price"=>null,
                "maxPublic"=>null,
                "image"=>"halloween.jpg"
            ],
            [
                "title"=>"Stage de canoé kayak",
                "dateEvent"=>new \Datetime("2021-05-10 8:00"),
                "description"=>"Le canoé kayak en plein coeur de paris ! Sur le canal Saint-Martin, pendant 3 jours, louvoyez agréablement entre les déchets et les pigeons.",
                "price"=>399,
                "maxPublic"=>3,
                "image"=>"canal.jpg"
            ],
            [
                "title"=>"Anniversaire de Chloé",
                "dateEvent"=>new \Datetime("2021-03-8 21:00"),
                "description"=>"Chloé fête ses 20 ans ! Rendez-vous chez elle pour lui faire une belle suprise. Soyez nombreux !!",
                "price"=>null,
                "maxPublic"=>null,
                "image"=>"soiree.jpg"
            ],
        ];

        foreach($users as $u){
            $user=new User();
            $user->setUsername($u["username"])
                ->setRoles($u["roles"])
                ->setPassword($this->encoder->encodePassword($user, $u["password"]));
            $manager->persist($user);
        }

        foreach($events as $e){
            dump($e["dateEvent"]);
            $event = new Event();
            $event->setTitle($e["title"])
                ->setDateEvent($e["dateEvent"])
                ->setDescription($e["description"])
                ->setPrice($e["price"])
                ->setMaxPublic($e["maxPublic"])
                ->setImage($e["image"]);
            
                $manager->persist($event);
        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
